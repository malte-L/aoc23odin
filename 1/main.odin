package main


import "core:fmt"
import "core:os"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part2())
}


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}


Tuple :: struct {
	name: string,
	digit: rune,
}

digit_name_to_num_mapping := [9]Tuple {
	{"one"  , '1'}
	{"eight", '8'}
	{"two"  , '2'}
	{"three", '3'}
	{"four" , '4'}
	{"five" , '5'}
	{"six"  , '6'}
	{"seven", '7'}
	{"nine" , '9'}
}


bytes :: #load("input.txt")


part1 :: proc() -> bool {
	string_it := string(bytes)
	sum: int

	for line in strings.split_lines_iterator(&string_it) {
		line := line


		nums := [100]rune{}
		i: int
		for r in line {
			if is_num[r] {
				nums[i] = r
				i += 1
			}
		}

		n1 := nums[0]
		n2 := nums[i - 1]

		b := strings.Builder{} // do i need a builder to concat two runes?
		defer strings.builder_destroy(&b)
		strings.write_rune(&b, n1)
		strings.write_rune(&b, n2)
		sum += strconv.parse_int(strings.to_string(b)) or_return
	}

	fmt.println(sum)
	return true
}

part2 :: proc() -> bool {
	string_it := string(bytes)


	sum: int

	for line in strings.split_lines_iterator(&string_it) {
		line := line

		n1 := first_digit(line)
		n2 := last_digit(line)


		b := strings.Builder{}
		defer strings.builder_destroy(&b)
		strings.write_rune(&b, n1)
		strings.write_rune(&b, n2)
		x := strconv.parse_int(strings.to_string(b)) or_return
		sum += x

		fmt.println(line, "::", strings.to_string(b), sum) // ouput seems fine to me
	}

	fmt.println(sum)

	return true




	first_digit :: proc(input: string) -> (x:rune) {
		input := input


		for input != "" {
			if r := utf8.rune_at(input, 0); is_num[r] {
				return r
			}

			for t in digit_name_to_num_mapping {
				if strings.has_prefix(input, t.name) {
					return t.digit
				}
			}
			input = input[1:]
		}

		panic(#procedure + "not found")
	}
	 
	last_digit :: proc(input: string) -> (x: rune) {
		input := input


		for input != "" {
			r, _ := utf8.decode_last_rune(input)
			if  is_num[r] {
				return r
			}

			for t in digit_name_to_num_mapping {
				if strings.has_suffix(input, t.name) {
					return t.digit
				}
			}
			input = input[:len(input) - 1]	
		}

		return
	}
}
