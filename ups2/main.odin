package main

import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"

main :: proc() {
	part2()
}


input :: string(#load("input.txt"))


part1 :: proc() -> (ok: bool) {
	lines_iter := input
	sum: int
	for line in strings.split_lines_iterator(&lines_iter) {
		dimensions_as_strings := strings.split_multi(line, {"x"})
		l := strconv.parse_int(dimensions_as_strings[0]) or_return
		w := strconv.parse_int(dimensions_as_strings[1]) or_return
		h := strconv.parse_int(dimensions_as_strings[2]) or_return

		face1 := l * w
		face2 := w * h
		face3 := h * l
		smallest_face := math.min(face1, math.min(face2, face3))
		sum += 2 * face1 + 2 * face2 + 2 * face3 + smallest_face
	}

	fmt.println(sum)
	return true
}


part2 :: proc() -> (ok: bool) {
	lines_iter := input
	sum: int
	for line in strings.split_lines_iterator(&lines_iter) {
		dimensions_as_strings := strings.split_multi(line, {"x"})
		l := strconv.parse_int(dimensions_as_strings[0]) or_return
		w := strconv.parse_int(dimensions_as_strings[1]) or_return
		h := strconv.parse_int(dimensions_as_strings[2]) or_return


		side1 := math.min(l, math.min(w, h))
		side2: int
		if side1 == l do side2 = math.min(w, h)
		if side1 == w do side2 = math.min(l, h)
		if side1 == h do side2 = math.min(w, l)
		sum += 2 * side1 + 2 * side2 + l * w * h
	}

	fmt.println(sum)
	return true
}
