package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:slice"
import "core:sort"
import "core:strconv"
import "core:strings"
import "core:time"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part2())

}


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}


Dir :: enum {
	L,
	R,
}

Map :: struct {
	dir_sequence: [dynamic]Dir,
	network:      map[string][Dir]string,
}


test_input := Map {
	dir_sequence = {.R, .L},
	network =  {
		"AAA" = {.L = "BBB", .R = "CCC"},
		"BBB" = {.L = "DDD", .R = "EEE"},
		"CCC" = {.L = "ZZZ", .R = "GGG"},
		"DDD" = {.L = "DDD", .R = "DDD"},
		"EEE" = {.L = "EEE", .R = "EEE"},
		"GGG" = {.L = "GGG", .R = "GGG"},
		"ZZZ" = {.L = "ZZZ", .R = "ZZZ"},
	},
}

test_input_1 := Map {
	dir_sequence = {.L, .L, .R},
	network =  {
		"AAA" = {.L = "BBB", .R = "BBB"},
		"BBB" = {.L = "AAA", .R = "ZZZ"},
		"ZZZ" = {.L = "ZZZ", .R = "ZZZ"},
	},
}


parse_input :: proc(using m: ^Map) {
	input :: #load("input.txt", string)
	line_iter := input


	i: int
	for line in strings.split_lines_iterator(&line_iter) {
		defer i += 1
		if i == 0 {
			for r, j in line {
				switch r {
				case 'L':
					append(&dir_sequence, Dir.L)
				case 'R':
					append(&dir_sequence, Dir.R)
				}
			}
			continue
		}

		if line == "" {
			continue
		}

		parts := strings.split_multi(line, {" = "})
		state := parts[0]
		directions := strings.split_multi(parts[1], {", "})
		network[state] = {
			.L = strings.trim_prefix(directions[0], "("),
			.R = strings.trim_suffix(directions[1], ")"),
		}

	}
}


part1 :: proc() -> (ok: bool) {

	m: Map
	parse_input(&m)

	fmt.println(walk_map(m))

	return true


	walk_map :: proc(using m: Map) -> (steps: int) {
		state := "AAA"
		for {
			if state == "ZZZ" do break
			dir := dir_sequence[steps % len(dir_sequence)]
			state = network[state][dir]
			steps += 1
		}
		return
	}

}


part2 :: proc() -> (ok: bool) {

	m: Map
	parse_input(&m)

	fmt.println(walk_map(m))

	return true


	walk_map :: proc(using m: Map) -> (steps: int) {
		fmt.println(len(network))

		states: [dynamic]string
		defer delete(states)

		for state, _ in network {
			if strings.has_suffix(state, "A") do append(&states, state)
		}

		fmt.println(states)


		for {
			all_states_are_end := true
			for state in states do all_states_are_end &&= strings.has_suffix(state, "Z")
			if all_states_are_end do break
			for state, i in states {
				dir := dir_sequence[steps % len(dir_sequence)]
				states[i] = network[state][dir]
			}

			steps += 1
		}
		return
	}

}
