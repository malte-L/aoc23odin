package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:slice"
import "core:sort"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part1())
}


ordering := map[rune]int {
	'A' = 14,
	'K' = 13,
	'Q' = 12,
	'T' = 10,
	'9' = 9,
	'8' = 8,
	'7' = 7,
	'6' = 6,
	'5' = 5,
	'4' = 4,
	'3' = 3,
	'2' = 2,
	'J' = 1,
}


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}


Hand :: struct {
	cards: [5]rune,
	bid:   int,
}


parse_input_into :: proc(hands: ^[dynamic]Hand) -> bool {
	input :: #load("input.txt", string)
	line_iter := input
	for line in strings.split_lines_iterator(&line_iter) {
		hand: Hand
		j: int
		for r, i in line {
			if r == ' ' do break
			hand.cards[i] = r
			j += 1
		}
		assert(j == 5)
		hand.bid = strconv.parse_int(line[j + 1:]) or_return
		append(hands, hand)
	}
	return true
}

part1 :: proc() -> (ok: bool) {
	hands: [dynamic]Hand
	defer delete(hands)

	parse_input_into(&hands) or_return


	slice.sort_by(hands[:], compare_hands)

	sum: int
	for hand, i in hands {
		sum += (i + 1) * hand.bid
		fmt.println(hand.cards, (i + 1) * hand.bid, hand.bid)
	}

	fmt.println(sum)

	return true


	compare_hands :: proc(h1, h2: Hand) -> (less: bool) {
		c1: map[rune]int
		c2: map[rune]int
		for r in h1.cards do c1[r] += 1
		for r in h2.cards do c2[r] += 1
		defer delete(c1)
		defer delete(c2)


		if len(c1) < len(c2) do return false
		if len(c1) > len(c2) do return true


		m1: int
		m2: int
		for _, i in c1 do m1 = math.max(m1, i)
		for _, i in c2 do m2 = math.max(m2, i)


		if m1 < m2 do return true
		if m1 > m2 do return false


		for i in 0 ..< 5 {
			if h1.cards[i] != h2.cards[i] {
				return ordering[h1.cards[i]] < ordering[h2.cards[i]]
			}
		}

		return false
	}
}


part2_attempt :: proc() -> (ok: bool) {
	hands: [dynamic]Hand
	defer delete(hands)

	parse_input_into(&hands) or_return


	slice.sort_by(hands[:], compare_hands)

	sum: int
	for hand, i in hands {
		sum += (i + 1) * hand.bid
		fmt.println(hand.cards, (i + 1) * hand.bid, hand.bid)
	}

	fmt.println(sum)

	return true


	compare_hands :: proc(h1, h2: Hand) -> (less: bool) {
		c1: map[rune]int
		c2: map[rune]int
		for r in h1.cards do c1[r] += 1
		for r in h2.cards do c2[r] += 1
		defer delete(c1)
		defer delete(c2)


		if len(c1) != len(c2) {
			fmt.println(h1.cards, h2.cards, c1, c2)
		}


		if len(c1) < len(c2) do return false
		if len(c1) > len(c2) do return true


		num_j1 := c1['J']
		num_j2 := c2['J']

		delete_key(&c1, 'J')
		delete_key(&c2, 'J')

		m1: int
		m2: int
		for _, i in c1 do m1 = math.max(m1, i)
		for _, i in c2 do m2 = math.max(m2, i)

		m1 += num_j1
		m2 += num_j2

		if m1 < m2 do return true
		if m1 > m2 do return false


		for i in 0 ..< 5 {
			if h1.cards[i] != h2.cards[i] {
				return ordering[h1.cards[i]] < ordering[h2.cards[i]]
			}
		}

		return false
	}
}
