package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part2())
}


input :: #load("input.txt", string)


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}

Race :: struct {
	time, distance: int,
}


part1 :: proc() -> (ok: bool) {


	test_races := [3]Race{{7, 9}, {15, 40}, {30, 200}}
	_ = test_races


	input := [4]Race{{44, 283}, {70, 1134}, {70, 1134}, {80, 1491}}


	product := 1

	for race in input {
		num_winners_for_race: int

		for time_button_is_pressed in 1 ..< race.time {
			time_left := (race.time - time_button_is_pressed)
			speed := time_button_is_pressed
			distance := time_left * speed
			fmt.println("distance", distance, "is winner", distance > race.distance)
			num_winners_for_race += int(distance > race.distance)
		}

		if num_winners_for_race <= 0 do continue
		product *= num_winners_for_race
	}
	fmt.println("result", product)


	return true
}

part2 :: proc() -> (ok: bool) {

	input := [1]Race{{44707080, 283113411341491}}


	product := 1

	for race in input {
		num_winners_for_race: int

		for time_button_is_pressed in 1 ..< race.time {
			time_left := (race.time - time_button_is_pressed)
			speed := time_button_is_pressed
			distance := time_left * speed
			num_winners_for_race += int(distance > race.distance)
		}

		if num_winners_for_race <= 0 do continue
		product *= num_winners_for_race
	}
	fmt.println("result", product)


	return true
}
