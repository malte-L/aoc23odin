package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part1())
}


input :: #load("input.txt", string)


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}


part1 :: proc() -> (ok: bool) {
	test_input := #load("input2.txt", string)


	line_iter := input


	sum: int

	for line in strings.split_lines_iterator(&line_iter) {
		card_seperator_index := strings.index(line, ":")
		line := line
		line = line[card_seperator_index + 1:]


		card_set_seperator_index := strings.index(line, "|")
		winning_cards := strings.fields(line[:card_set_seperator_index])
		cards_i_have := strings.fields(line[card_set_seperator_index + 1:])

		found_count: uint
		for winning_card in winning_cards {
			for card_i_have in cards_i_have {
				if winning_card == card_i_have do found_count += 1
			}
		}

		if found_count == 0 do continue
		sum += 1 << (found_count - 1)
	}


	fmt.println(sum)

	return true
}

part2 :: proc() -> (ok: bool) {
	test_input := #load("input2.txt", string)


	line_iter := input


	num_card_copies: [1000]int

	for line in strings.split_lines_iterator(&line_iter) {
		card_seperator_index := strings.index(line, ":")
		line := line

		card_id := strconv.parse_uint(
			strings.trim_space(line[len("Game"):card_seperator_index]),
		) or_return


		line = line[card_seperator_index + 1:]


		card_set_seperator_index := strings.index(line, "|")
		winning_cards := strings.fields(line[:card_set_seperator_index])
		cards_i_have := strings.fields(line[card_set_seperator_index + 1:])

		found_count: uint
		for winning_card in winning_cards {
			for card_i_have in cards_i_have {
				if winning_card == card_i_have do found_count += 1
			}
		}


		num_card_copies[card_id] += 1

		for copies in 0 ..< num_card_copies[card_id] {
			for i in 0 ..< found_count {
				num_card_copies[card_id + i + 1] += 1
			}
		}
		//delete_key(&num_card_copies, card_id)
	}


	sum: int
	for copies_left, index in num_card_copies {
		if copies_left == 0 do continue
		//fmt.println("card:", index, "copies left", copies_left)
		sum += copies_left
	}


	fmt.println(sum)

	return true
}
