package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part2())
}


input :: #load("input.txt", string)


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}



// too naive
Mapping :: distinct map[int]int

part1 :: proc(use_test_input := false) -> (ok: bool) {
	test_input := #load("input.txt", string)

	if use_test_input {
		m1: Mapping
		insert_ranged_mapping(&m1, 50, 98, 2)
		insert_ranged_mapping(&m1, 52, 50, 48)

		assert(lookup_mapping(m1, 98) == 50)
		assert(lookup_mapping(m1, 53) == 55)
		assert(lookup_mapping(m1, 10) == 10)

		m2:= []Ranged_Mapping {{50, 98, 2}, {52, 50, 48}}
		assert(lookup_ranged_mapping_multi(m2, 98) == 50)
		assert(lookup_ranged_mapping_multi(m2, 53) == 55)
		assert(lookup_ranged_mapping_multi(m2, 10) == 10)
	}


	lines := strings.split_multi(test_input, {"\n"})
	line_index: int


	seeds := strings.fields(lines[line_index][len("seeds: "):])
	line_index += 1

	fmt.println(seeds)

	assert(lines[line_index] == "");line_index += 1
	assert(lines[line_index] == "seed-to-soil map:");line_index += 1

	seed_to_soil: [dynamic]Ranged_Mapping
	fmt.println("seed_to_soil")
	read_complete_ranged_mapping(&seed_to_soil, lines, &line_index)

	soil_to_fertilizer: [dynamic]Ranged_Mapping
	fmt.println("soil_to_fertilizer")
	read_complete_ranged_mapping(&soil_to_fertilizer, lines, &line_index)

	fertilizer_to_water: [dynamic]Ranged_Mapping
	fmt.println("fertilizer_to_water")
	read_complete_ranged_mapping(&fertilizer_to_water, lines, &line_index)

	water_to_light: [dynamic]Ranged_Mapping
	fmt.println("water_to_light")
	read_complete_ranged_mapping(&water_to_light, lines, &line_index)

	light_to_temperatur: [dynamic]Ranged_Mapping
	fmt.println("light_to_temperatur")
	read_complete_ranged_mapping(&light_to_temperatur, lines, &line_index)

	temperatur_to_humidity: [dynamic]Ranged_Mapping
	fmt.println("temperatur_to_humidity")
	read_complete_ranged_mapping(&temperatur_to_humidity, lines, &line_index)

	humidity_to_location: [dynamic]Ranged_Mapping
	fmt.println("humidity_to_location")
	read_complete_ranged_mapping(&humidity_to_location, lines, &line_index)




	min := math.max(int)
	for seed_string in seeds {
		seed_int := strconv.parse_int(seed_string) or_return
		fmt.println("checking seed:", seed_int, seed_string)
		min = math.min(
			min,
			lookup_ranged_mapping_multi(
				humidity_to_location[:],
				lookup_ranged_mapping_multi(
					temperatur_to_humidity[:],
					lookup_ranged_mapping_multi(
						light_to_temperatur[:],
						lookup_ranged_mapping_multi(
							water_to_light[:],
							lookup_ranged_mapping_multi(
								fertilizer_to_water[:],
								lookup_ranged_mapping_multi(
									soil_to_fertilizer[:],
									lookup_ranged_mapping_multi(seed_to_soil[:], seed_int),
								),	// oof
							),
						),
					),
				),
			),
		)
	}


	fmt.println("min:", min) // is 278755257


	return true

}


part2 :: proc() -> (ok: bool) {
	test_input := #load("input.txt", string)


	lines := strings.split_multi(test_input, {"\n"})
	line_index: int


	seed_range_pairs := strings.fields(lines[line_index][len("seeds: "):])
	assert(len(seed_range_pairs) % 2 == 0)
	line_index += 1


	assert(lines[line_index] == "");line_index += 1
	assert(lines[line_index] == "seed-to-soil map:");line_index += 1

	seed_to_soil: [dynamic]Ranged_Mapping
	fmt.println("seed_to_soil")
	read_complete_ranged_mapping(&seed_to_soil, lines, &line_index)

	soil_to_fertilizer: [dynamic]Ranged_Mapping
	fmt.println("soil_to_fertilizer")
	read_complete_ranged_mapping(&soil_to_fertilizer, lines, &line_index)

	fertilizer_to_water: [dynamic]Ranged_Mapping
	fmt.println("fertilizer_to_water")
	read_complete_ranged_mapping(&fertilizer_to_water, lines, &line_index)

	water_to_light: [dynamic]Ranged_Mapping
	fmt.println("water_to_light")
	read_complete_ranged_mapping(&water_to_light, lines, &line_index)

	light_to_temperatur: [dynamic]Ranged_Mapping
	fmt.println("light_to_temperatur")
	read_complete_ranged_mapping(&light_to_temperatur, lines, &line_index)

	temperatur_to_humidity: [dynamic]Ranged_Mapping
	fmt.println("temperatur_to_humidity")
	read_complete_ranged_mapping(&temperatur_to_humidity, lines, &line_index)

	humidity_to_location: [dynamic]Ranged_Mapping
	fmt.println("humidity_to_location")
	read_complete_ranged_mapping(&humidity_to_location, lines, &line_index)




	min := math.max(int)


	i: int
	for {
		if i >= len(seed_range_pairs) do break

		seed_start := strconv.parse_int(seed_range_pairs[i]) or_return
		seed_range := strconv.parse_int(seed_range_pairs[i+1]) or_return
		fmt.println("checking", seed_range , "values")
		defer i+= 2


	 	for seed_int in seed_start..<seed_start+seed_range	{

	 		mapped := lookup_ranged_mapping_multi(
					humidity_to_location[:],
					lookup_ranged_mapping_multi(
						temperatur_to_humidity[:],
						lookup_ranged_mapping_multi(
							light_to_temperatur[:],
							lookup_ranged_mapping_multi(
								water_to_light[:],
								lookup_ranged_mapping_multi(
									fertilizer_to_water[:],
									lookup_ranged_mapping_multi(
										soil_to_fertilizer[:],
										lookup_ranged_mapping_multi(seed_to_soil[:], seed_int),
									),	// oof
								),
							),
						),
					),
				)

	 		// should be faster ?
	 		next_min := [2]int{
	 			0 = min,
	 			1 = mapped,
	 		}

			min = next_min[int(mapped < min)]
		}
	}



	fmt.println("min:", min)


	return true
}


insert_ranged_mapping :: proc(
	mapping: ^Mapping,
	destination_start, source_start, range: int,
	allocator := context.allocator,
) {
	for source, i in source_start ..< source_start + range {
		fmt.println(source)
		mapping[source] = destination_start + i
	}
	return
}


lookup_mapping :: proc(m: Mapping, source_value: int) -> (destination_value: int) {
	return m[source_value] or_else source_value
}


Ranged_Mapping :: struct {
	destination_start, source_start, range: int,
}


lookup_ranged_mapping :: proc {
	lookup_ranged_mapping_single,
	lookup_ranged_mapping_multi
}

lookup_ranged_mapping_single :: proc(
	using rm: Ranged_Mapping,
	source_value: int,
) -> (
	destination_value: int, ok :bool
) {
	if source_value < source_start || source_value >= source_start + range do return source_value, false
	return destination_start + (source_value - source_start), true
}


lookup_ranged_mapping_multi :: proc(rms: []Ranged_Mapping, source_value: int) ->  (
	destination_value: int, ok :bool,
) #optional_ok {
	for rm in rms {
		destination_value, value_was_in_range := lookup_ranged_mapping_single(rm, source_value)
		if value_was_in_range do return destination_value, true
	}
	return source_value, false
}



read_ranged_mapping_values :: proc(
	line: string,
	allocator := context.allocator,
) -> (
	destination_start, source_start, range: int,
	ok: bool,
) {
	if line == "" {
		ok = false
		return
	}

	fields := strings.fields(line, allocator)
	fmt.println(fields)
	assert(len(fields) == 3)
	destination_start = strconv.parse_int(fields[0]) or_return
	source_start = strconv.parse_int(fields[1]) or_return
	range = strconv.parse_int(fields[2]) or_return
	ok = true
	return
}

// deprecated
read_complete_mapping :: proc(
	m: ^Mapping,
	lines: []string,
	line_index: ^int,
	allocator := context.allocator,
) {
	for {
		fmt.println("one")
		if line_index^ >= len(lines) do break
		d, s, r, reading_ok := read_ranged_mapping_values(
			lines[line_index^],
			allocator,
		);line_index^ += 1
		fmt.println("two")
		if !reading_ok do break
		fmt.println("three")
		insert_ranged_mapping(m, d, s, r)
	}
	line_index^ += 1 // skip whitespace
}

read_complete_ranged_mapping :: proc(
	m: ^[dynamic]Ranged_Mapping,
	lines: []string,
	line_index: ^int,
	allocator := context.allocator,
) {
	for {
		fmt.println("one")
		if line_index^ >= len(lines) do break
		d, s, r, reading_ok := read_ranged_mapping_values(
			lines[line_index^],
			allocator,
		);line_index^ += 1
		fmt.println("two")
		if !reading_ok do break
		fmt.println("three")
		append(m, Ranged_Mapping{d, s, r})
	}
	line_index^ += 1 // skip whitespace
}