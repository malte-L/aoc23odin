package main

import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"

main :: proc() {
	part2_improved()
}


input :: #load("input.txt", string)


part1 :: proc() -> (ok: bool) {
	lines_iter := input

	highest_allowed_amount_per_color := map[string]int {
		"red"   = 12,
		"green" = 13,
		"blue"  = 14,
	}

	sum: int
	for line in strings.split_lines_iterator(&lines_iter) {
		line := line
		line = strings.trim_prefix(line, "Game ")
		game_id_delimiter_index := strings.index(line, ":")
		game_id := strconv.parse_int(line[:game_id_delimiter_index]) or_return
		semi_iter := line[game_id_delimiter_index + 1 + 1:]
		game_is_possible := true
		for round in strings.split_iterator(&semi_iter, "; ") {
			comma_iter := round
			for color in strings.split_iterator(&comma_iter, ", ") {
				space_index := strings.index(color, " ")
				amount := strconv.parse_int(color[:space_index]) or_return
				allowed_amount :=
					amount <= highest_allowed_amount_per_color[(color[space_index + 1:])]
				game_is_possible &&= allowed_amount
			}
		}
		if game_is_possible do sum += game_id
	}

	fmt.println(sum)
	return true
}


part2 :: proc() -> (ok: bool) {
	lines_iter := input

	sum: int
	for line in strings.split_lines_iterator(&lines_iter) {
		line := line
		game_id_delimiter_index := strings.index(line, ":")
		semi_iter := line[game_id_delimiter_index + len(": "):]
		minimum_amount_required_per_color: map[string]int
		for round in strings.split_iterator(&semi_iter, "; ") {
			comma_iter := round
			for color in strings.split_iterator(&comma_iter, ", ") {
				space_index := strings.index(color, " ")
				amount := strconv.parse_int(color[:space_index]) or_return
				color_string := color[space_index + 1:]
				minimum_amount_required_per_color[color_string] = math.max(
					minimum_amount_required_per_color[color_string],
					amount,
				)
			}
		}
		power := 1
		for _, it in minimum_amount_required_per_color do power *= it
		sum += power
	}
	fmt.println(sum)
	return true
}


part2_improved :: proc() -> (ok: bool) {
	lines_iter := input

	sum: int
	for line in strings.split_lines_iterator(&lines_iter) {
		line := line
		game_id_delimiter_index := strings.index(line, ":")
		semi_iter := line[game_id_delimiter_index + len(": "):]
		minimum_amount_required_per_color: map[Color]int
		for round in strings.split_iterator(&semi_iter, "; ") {
			comma_iter := round
			for color in strings.split_iterator(&comma_iter, ", ") {
				space_index := strings.index(color, " ")
				amount := strconv.parse_int(color[:space_index]) or_return
				color_string := color_from_string_or_panic(color[space_index + 1:])
				minimum_amount_required_per_color[color_string] = math.max(
					minimum_amount_required_per_color[color_string],
					amount,
				)
			}
		}
		power := 1
		for _, it in minimum_amount_required_per_color do power *= it
		sum += power
	}
	fmt.println(sum)
	return true
}

Color :: enum {
	Red, Green, Blue
}

color_from_string_or_panic :: proc(str: string) -> Color { 
	switch strings.trim_space(str) {
		case "blue": return .Blue
		case "red": return .Red
		case "green": return .Green
	}
	panic(fmt.tprint(#procedure, str))
}
