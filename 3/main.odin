package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:strconv"
import "core:strings"
import "core:unicode/utf8"

main :: proc() {
	part2()
}


input :: #load("input.txt", string)


Num :: struct {
	raw:   [dynamic]rune,
	start: [2]int,
}


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}

SYMBOL_MAP_SIZE :: 1000


part1 :: proc() -> (ok: bool) {
	symbol_map: [SYMBOL_MAP_SIZE][SYMBOL_MAP_SIZE]bool


	test_input := #load("input2.txt", string)


	test_input_2 :: `....+
.12..
.....
`


	nums: [dynamic]Num


	line_iter := input

	y: int
	for line in strings.split_lines_iterator(&line_iter) {
		defer y += 1
		last_was_num: bool
		n: Num
		for r, x in line {
			if is_num[r] {
				if !last_was_num {
					n = Num {
						start = {x, y},
					}
					last_was_num = true
				}

				append(&n.raw, r)
				continue
			}
			if last_was_num {
				append(&nums, n)
				last_was_num = false
			}
			if r == '.' do continue
			symbol_map[x][y] = true
		}
		// checking for literal edge-case
		if last_was_num {
			append(&nums, n)
			last_was_num = false
		}
	}


	if false {
		for x in 0 ..< SYMBOL_MAP_SIZE {
			for y in 0 ..< SYMBOL_MAP_SIZE {
				if symbol_map[x][y] do fmt.println(x, y)
			}
		}
	}

	sum: int
	// check num neighbours
	for num in nums {
		using num

		/*
			23456

			1
			2
			1
	

		*/


		map_search_loop: for x in max(
			0,
			start.x - 1,
		) ..= min(SYMBOL_MAP_SIZE, start.x + len(raw)) {
			for y in max(0, start.y - 1) ..= min(SYMBOL_MAP_SIZE, start.y + 1) {
				if symbol_map[x][y] {
					b: strings.Builder
					defer strings.builder_destroy(&b)
					for r in raw do strings.write_rune(&b, r)
					num_as_int := strconv.parse_int(strings.to_string(b)) or_return
					fmt.println(num_as_int)
					sum += num_as_int
					break map_search_loop
				}
			}
		}
	}


	fmt.println(sum)

	return true
}


symbol_map_2: [SYMBOL_MAP_SIZE][SYMBOL_MAP_SIZE]bool
symbol_map_counter: [SYMBOL_MAP_SIZE][SYMBOL_MAP_SIZE]int


// this solution is not really that great but i wanted to reuse the approach from part1 (bc time..)
part2 :: proc() -> (ok: bool) {


	test_input := #load("input2.txt", string)


	test_input_2 :: `....+
.12..
.....
`


	nums: [dynamic]Num


	line_iter := input

	y: int
	for line in strings.split_lines_iterator(&line_iter) {
		defer y += 1
		last_was_num: bool
		n: Num
		for r, x in line {
			if is_num[r] {
				if !last_was_num {
					n = Num {
						start = {x, y},
					}
					last_was_num = true
				}

				append(&n.raw, r)
				continue
			}
			if last_was_num {
				append(&nums, n)
				last_was_num = false
			}

			if r == '*' do symbol_map_2[x][y] = true

		}
		// checking for literal edge-case
		if last_was_num {
			append(&nums, n)
			last_was_num = false
		}
	}


	sum: int
	// check num neighbours
	for num in nums {
		using num

		/*
			23456

			1
			2
			1
	

		*/


		map_search_loop: for x in max(
			0,
			start.x - 1,
		) ..= min(SYMBOL_MAP_SIZE, start.x + len(raw)) {
			for y in max(0, start.y - 1) ..= min(SYMBOL_MAP_SIZE, start.y + 1) {
				if symbol_map_2[x][y] {
					b: strings.Builder
					defer strings.builder_destroy(&b)
					for r in raw do strings.write_rune(&b, r)
					num_as_int := strconv.parse_int(strings.to_string(b)) or_return
					fmt.println(num_as_int)

					if symbol_map_counter[x][y] > 0 {
						sum += symbol_map_counter[x][y] * num_as_int
					} else {
						symbol_map_counter[x][y] = num_as_int
					}

					break map_search_loop
				}
			}
		}
	}


	fmt.println(sum)

	return true
}
