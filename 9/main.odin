package main


import "core:bytes"
import "core:fmt"
import "core:math"
import "core:slice"
import "core:sort"
import "core:strconv"
import "core:strings"
import "core:time"
import "core:unicode/utf8"

main :: proc() {
	fmt.println(part2())

}


is_num := map[rune]bool {
	'0' = true,
	'1' = true,
	'2' = true,
	'3' = true,
	'4' = true,
	'5' = true,
	'6' = true,
	'7' = true,
	'8' = true,
	'9' = true,
}


input :: #load("input.txt", string)


N :: 21

part1 :: proc() -> (ok: bool) {

	value_histories: [dynamic][N]int
	parse_input(&value_histories) or_return

	sum: int
	for xs in value_histories {
		lasts: [dynamic]int
		append(&lasts, xs[len(xs) - 1])

		xs := xs

		step := 1
		for {
			all_z := true
			for i in 0 ..< len(xs) - step {
				xs[i] = xs[i + 1] - xs[i]
				all_z &&= xs[i] == 0
				fmt.println(xs[i], all_z)
			}

			fmt.println(xs, xs[len(xs) - step - 1])

			append(&lasts, xs[len(xs) - step - 1])
			step += 1
			if all_z do break
		}

		next_value: int
		for l in lasts do next_value += l

		fmt.println(lasts, next_value)
		sum += next_value
	}


	fmt.println(sum)
	return true


	// 6 values on each line in input.txt
	parse_input :: proc(value_history: ^[dynamic][N]int) -> (ok: bool) {
		line_iter := input
		for line in strings.split_lines_iterator(&line_iter) {
			next_value_history: [N]int
			field_iter := line
			i: int
			for field in strings.fields_iterator(&field_iter) {
				defer i += 1
				next_value_history[i] = strconv.parse_int(field) or_return
			}
			append(value_history, next_value_history)
		}
		return true
	}

}

part2 :: proc() -> (ok: bool) {

	value_histories: [dynamic][N]int
	parse_input(&value_histories) or_return

	sum: int
	for xs in value_histories {
		firsts: [dynamic]int
		append(&firsts, xs[0])

		xs := xs

		step := 1
		for {
			all_z := true
			for i in 0 ..< len(xs) - step {
				xs[i] = xs[i + 1] - xs[i]
				all_z &&= xs[i] == 0
			}

			append(&firsts, xs[0])
			step += 1
			if all_z do break
		}

		next_value: int
		for l in 0 ..< len(firsts) {
			i := len(firsts) - l - 1
			next_value = firsts[i] - next_value

		}
		fmt.println(next_value)

		sum += next_value
	}


	fmt.println(sum)
	return true


	// 6 values on each line in input.txt
	parse_input :: proc(value_history: ^[dynamic][N]int) -> (ok: bool) {
		line_iter := input
		for line in strings.split_lines_iterator(&line_iter) {
			next_value_history: [N]int
			field_iter := line
			i: int
			for field in strings.fields_iterator(&field_iter) {
				defer i += 1
				next_value_history[i] = strconv.parse_int(field) or_return
			}
			append(value_history, next_value_history)
		}
		return true
	}

}
